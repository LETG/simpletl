.. SimpleETL documentation master file, created by
   sphinx-quickstart on Sun Feb 12 17:11:03 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SimpleETL's documentation!
============================================

.. toctree::
   :maxdepth: 2

.. include:: ../../README.md

How-to
======

Prerequisites: make sure the source and target shapefile layers are loaded into the QGIS project.

* Open the plugin window. The main window is divided in 3 columns:
   * Layer selection and source layer information
   * target layer information, processing options, and transformations rules
   * actions: save/load configuration file, start and cancel process, logs display
* Select the source and target layers from the upper left combobox. This will update tables with layer fields
* Choose processing options :
   * if you want to process geometries or not (only attributes)
   * if you want to update records based on a unique field, or just integrate new data
   * if you want to process all features or selected features only from source layer
* Define transformation rules:
   * for each field in the target layer, you can select a field from the source layer or use the field calculator for more complex rules. Once defined, you can use the `Save` button to save the rules in a JSON file.
   * or use the `Load` button on the upper right to load predefined configuration file (JSON format)
* Start the ETL process

Some messages may appear in the logs window, for information about loading transformation rules, results of ETL process, errors, etc.

.. image:: _static/images/main_window.png
   :width: 100 %
   :align: center

Transformation rules definition file
====================================

JSON files for saving/loading transformation rules are structured as follows:

.. code-block:: json
   
   {
      "parameters": {
         "use_geometry": true,
         "id_field": 0,
         "update_data": false,
         "all_features": true
      },
      "rules": {
         "0": "concat(code, '_', $id)",
         "1": "id"
      }
   }


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

